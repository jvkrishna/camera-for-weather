import os,datetime,csv, requests,time, schedule, logging,pytz,yaml
from xml.etree import ElementTree
from multiprocessing import Pool

# Logging configuration
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')


with open('application.yml') as f:
    config = yaml.safe_load(f)

# Constants
process_cameras = set([str(x) for x in config['cameras']])
camera_inventories = []
root_folder_path = config['images']['download']['folder']
download_freq = config['images']['download']['frequency']
url_string = config['images']['inventory']['url']

class CameraInventory:
    """
    Class to hold the camera inventory details.
    """
    def __init__(self,device_id,device_name,latitude,longitude,imageURL,videoURL):
        self.device_id = device_id
        self.device_name = device_name
        self.latitude = latitude
        self.longitude = longitude
        self.imageURL = imageURL
        self.videoURL =videoURL

    def __iter__(self):
        return iter([self.device_id,self.device_name,self.latitude,self.longitude])

    def __hash__(self):
        return hash((self.device_id,self.device_name))

    def __eq__(self, other):
        return (self.device_id,self.device_name) == (other.device_id,other.device_name)

    def __ne__(self, other):
        return not(self == other)

    def __str__(self):
        return '[{},{}]'.format(self.device_id,self.device_name)


def update_camera_inventory():
    """
    Downloads the camera inventory and creates in-memory objects
    :return:
    """
    logging.debug('Downloading the camera inventory')

    # Create in memory object of the camera feed
    try:
        global camera_inventories
        # Download Camera Invenotry Feed
        content = requests.get(url_string).content
        xml_root = ElementTree.fromstring(content)
        ns = {'tcore': 'http://www.tcoreAddOns.com'}
        temp = [[device.find('./device-inventory-header'), device.find("./tcore:cctv-data-url-list", ns)]
                for device in xml_root.findall('cctv-inventory-item')]
        camera_inventories = []
        for header, dataurl in temp:
            device_id, device_name, latitude, longitude = header.findtext('device-id'), header.findtext('device-name') \
                , header.findtext('./device-location/latitude'), header.findtext('./device-location/longitude')
            if device_id in process_cameras:
                imageURL, videoURL = dataurl.find('./tcore:static-image-url', ns).text, \
                                     dataurl.find('./tcore:live-video-url', ns).text if dataurl.find(
                                         './tcore:live-video-url', ns) else None
                camera_inventories.append(CameraInventory(device_id, device_name, latitude, longitude, imageURL, videoURL))

        logging.info('Total number of cameras downloaded: %s', len(camera_inventories))
    except:
        logging.error('Erorr updating the camera inventory')





def downloadCameraImages():
    """
    Download images for all cameras
    :return:
    """

    now = datetime.datetime.now()
    logging.info('Stating image download batch at ' + str(now))
    with Pool(4) as pool:
        pool.map(download_camera_image, camera_inventories)

    now = datetime.datetime.now()
    logging.info('Completed image download batch at ' + str(now))


def download_camera_image(camera):
    """
    Download the image for the given camera
    """
    now = datetime.datetime.now()
    now = now.replace(minute=download_freq * int(now.minute / download_freq))
    logging.info('Downloading camera images at ' + str(now))
    today_folder_path = now.strftime('%Y%m%d')
    now_file_path = now.strftime('%Y-%m-%d-%H-%M')
    logging.debug('Downloading images at %s', now_file_path)
    logging.debug('Downloading images for camera %s', camera)
    try:
        folder_path = os.path.join(root_folder_path, camera.device_id, today_folder_path)
        os.makedirs(folder_path, exist_ok=True)
        response = requests.get(camera.imageURL, timeout=2)
        if response.status_code == 200:
            with open(os.path.join(folder_path, now_file_path + '.jpg'), 'wb') as f:
                f.write(response.content)
    except Exception as e:
        logging.error('Image download failed for camera %s \n error: %s', camera, e)


if __name__ == '__main__':
    logging.debug('Started camera image downloader')
    update_camera_inventory()
    logging.info("Initialization completed.")
    downloadCameraImages()
    schedule.every().day.at("00:02").do(update_camera_inventory)
    schedule.every(download_freq).minutes.do(downloadCameraImages)
    while True:
        schedule.run_pending()
        time.sleep(1)

        # downloadCameraImages()