# Installation following dependencies:

pyyaml
schedule

# Configuration

Modify application.yml file for adding/removing the cameras.

# Running

Execute the camera_downloader.py file for downloading the images
